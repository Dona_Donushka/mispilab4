package com.example.persistence;

import com.example.model.Point;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.persistence.EntityManager;
import jakarta.persistence.Persistence;
import jakarta.persistence.PersistenceContext;
import jakarta.transaction.Transactional;

import java.sql.Timestamp;
import java.util.List;

//@Transactional
//@ApplicationScoped
public class PointService {
    private static PointService instance = null;
    //@PersistenceContext (unitName = "postgres")
    private final EntityManager em;

    private PointService() {
          em = Persistence.createEntityManagerFactory("postgres").createEntityManager();
    }

    public static PointService getInstance() {
        if (instance == null)
            instance = new PointService();

        return instance;
    }

    public List<Point> getAllPoints() {
        return em.createQuery("select p from Point p", Point.class).getResultList();
    }

    public List<Timestamp> getAllTime() { return em.createQuery("select p.clickTime from Point p", Timestamp.class).getResultList(); }

    public long getRecordsCount() {
        return (Long) em.createQuery("select count(p) from Point p").getSingleResult();
    }

    public long getMissedCount() {
        return (Long) em.createQuery("select count(p) from Point p where p.hit = false").getSingleResult();
    }

    public Point addPoint(Point point) {
        em.getTransaction().begin();
        em.persist(point);
        em.getTransaction().commit();
        return  point;
    }

    public void truncate() {
        em.getTransaction().begin();
        em.createNativeQuery("truncate table points").executeUpdate();
        em.getTransaction().commit();
    }

}
