package com.example.model;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import org.hibernate.annotations.GenericGenerator;

import java.sql.Timestamp;
import java.time.format.DateTimeFormatter;

@Entity
@Table (name = "Points")
public class Point {

    @Id
    @GeneratedValue
    private Long id;
    private Double x;
    private Double y;
    private Double r;
    private Boolean hit;
    @Column(nullable = false)
    private Timestamp clickTime;

    public Point(Double x, Double y, Double r){
        this.x = x;
        this.y = y;
        this.r = r;
    }

    public Point(Double x, Double y) {
        this(x, y, 10d);
    }

    public Point() {
    }

    public Double getR() {
        return r;
    }

    public void setR(Double r) {
        this.r = r;
    }

    public Long getId() {
        return id;
    }

    public Double getX() {
        return x;
    }

    public void setX(Double x) {
        this.x = x;
    }

    public Double getY() {
        return y;
    }

    public void setY(Double y) {
        this.y = y;
    }

    public Boolean getHit() {
        return hit;
    }

    public void setHit(Boolean hit) {
        this.hit = hit;
    }

    public Timestamp getClickTime(){ return clickTime; }
    public String getClickTimeString() {
        String timeColonPattern = "HH:mm:ss";
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(timeColonPattern);
        return formatter.format(clickTime.toLocalDateTime());
    }

    public void setClickTime(Timestamp clickTime){ this.clickTime = clickTime; }
}
