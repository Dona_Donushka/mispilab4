package com.example.buisness;

import com.example.model.Point;
import jakarta.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class CheckPoint {

    public CheckPoint(){

    }

    public boolean check(Point point){
        return     checkIIQuadrant(point)
                || checkIIIQuadrant(point)
                || checkIVQuadrant(point);

    }


    private boolean checkIIQuadrant(Point point){

        return point.getX() <= 0.0  &&
                point.getY() >= 0.0 &&
                point.getX() * point.getX() + point.getY() * point.getY() <= point.getR() * point.getR();

    }

    private boolean checkIIIQuadrant(Point point) {

        return point.getX() <= 0.0 && point.getX() >= -point.getR() &&
                point.getY() <= 0.0 && point.getY() >= -point.getR();
    }

    private boolean checkIVQuadrant(Point point) {

        return point.getX() >= 0.0 && point.getX() <= point.getR()/2 // Проверка на 4 квадрант
                && point.getY() <= 0 && point.getY() >= -point.getR()/2
                && point.getY () >= point.getX() - point.getR()/2;
    }
}
