package com.example.mbeans;

import javax.management.Notification;
import javax.management.NotificationBroadcasterSupport;

public class SendInfo extends NotificationBroadcasterSupport implements SendInfoMBean {

    private final String TYPE = "Warning";
    private long sequenceNumber = 0;
    private final String MESSAGE = "Point's coordinate are out of range";

    @Override
    public void sendNotification() {
        sequenceNumber++;
        Notification notification = new Notification(TYPE, this, sequenceNumber, MESSAGE);
        sendNotification(notification);
    }

}
