package com.example.mbeans;

public interface AverageTimeMBean {
    long getAverage();
}
