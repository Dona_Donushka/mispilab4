package com.example.mbeans;

import com.example.persistence.PointService;
import jakarta.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class CountHits implements CountHitsMBean {
    //@Inject
    private final PointService pointService = PointService.getInstance();

    @Override
    public long getCount() {
        System.out.println("Call getCount() function");
        return pointService.getRecordsCount();
    }

    @Override
    public long getMisses() {
        System.out.println("Call getMisses() function");
        return pointService.getMissedCount();
    }
}
