package com.example.mbeans;

import com.example.persistence.PointService;

import java.sql.Timestamp;
import java.util.List;

public class AverageTime implements AverageTimeMBean {

    //@Inject
    private final PointService pointService = PointService.getInstance();

    @Override
    public long getAverage() {
        // get list
        List<Timestamp> allTime = pointService.getAllTime();
        if (allTime.size() < 2) {
            return 0;
        }
        int amountOfIntervals = allTime.size() - 1;

        // calculate average
        long period = 0;
        for (int i = 0; i < amountOfIntervals; i++) {
            Timestamp first = allTime.get(i);
            Timestamp second = allTime.get(i + 1);
            if(first == null || second == null) {
                continue;
            }

            period += Math.abs((first.getTime() - second.getTime()));
        }

        return period / amountOfIntervals / 1000;
    }
}
