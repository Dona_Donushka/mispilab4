package com.example.mbeans;

public interface CountHitsMBean {
    long getCount();

    long getMisses();
}
