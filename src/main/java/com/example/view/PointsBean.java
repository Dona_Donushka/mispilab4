package com.example.view;

import com.example.buisness.CheckPoint;
import com.example.mbeans.AverageTime;
import com.example.mbeans.CountHits;
import com.example.mbeans.SendInfo;
import com.example.model.Point;
import com.example.persistence.PointService;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.inject.Named;

import javax.management.MBeanServer;
import javax.management.ObjectName;
import java.io.Serializable;
import java.lang.management.ManagementFactory;
import java.sql.Timestamp;
import java.util.List;


@Named
@ApplicationScoped
public class PointsBean implements Serializable {

    private double x = 0d;
    private double y = 0d;
    private double r = 3.0d;

    private List<Point> pointsCollection = null;
    private final int offset = 0;

    private final Point pointField = new Point();
    private final Point pointGraphic = new Point();
    private long missed = 0;
    private long shots = 0;
    private final String square = "0";
    private final String message="";

    //=============== DB SERVICE ========================
    private final PointService pointService = PointService.getInstance();

    //================ MBEANS  activation =====================
    private static final AverageTime averageTime = new AverageTime();
    private static final CountHits countHits = new CountHits();
    private static final SendInfo sendInfo = new SendInfo();
//    static {
//        try {
//            MBeanServer mbs = ManagementFactory.getPlatformMBeanServer();
//            ObjectName myPersonalBean = new ObjectName("itmo.mispi.lab4:type=AverageTime");
//            ObjectName myPersonalBean2 = new ObjectName("itmo.mispi.lab4:type=CountHits");
//            ObjectName myPersonalBean3 = new ObjectName("itmo.mispi.lab4:type=SendInfo");
//            mbs.registerMBean(averageTime, myPersonalBean);
//            mbs.registerMBean(countHits, myPersonalBean2);
//            mbs.registerMBean(sendInfo, myPersonalBean3);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//    }

    //================== Checker ==============================
    @Inject
    private CheckPoint checkPoint;

    //================== GETTERS & SETTERS ===================

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public double getR() {
        return r;
    }

    public void setR(double r) {
        this.r = r;
    }

    public Point getPointField() {
        return pointField;
    }

    public long getShots() {
        return shots;
    }

    public long getMissed() {
        return missed;
    }

    public String getSquare() {
        return square;
    }

    public String getMessage() {
        return message;
    }

    public int getOffset() {
        return offset;
    }

    public List<Point> getPointsCollection() {
        return pointsCollection;
    }

    public Point getPointGraphic() {
        return pointGraphic;
    }

    //==========================================================
    public void uploadPoints() {
        pointsCollection = pointService.getAllPoints();
        shots = pointService.getRecordsCount();
        missed = pointService.getMissedCount();
    }

    public void submitFieldPoints() {
        try {
            Point point = new Point(x, y, r);

            if (Math.abs(point.getX()) > r || Math.abs(point.getY()) > r)
                sendInfo.sendNotification();

            Timestamp currentTimestamp = new Timestamp(System.currentTimeMillis());
            point.setClickTime(currentTimestamp);
            boolean result = checkPoint.check(point);

            // set point.hit
            point.setHit(result);
            // save the point into DB
            pointService.addPoint(point);
        } catch (NumberFormatException e) {
            // TODO
        }
    }

    public void submitGraphicPoints() {

    }

    public void submitR() {

    }

    public String getDateOffset(int offset, String date) {

        return date;
    }

    public void clear() {
        pointService.truncate();
    }


    private void addPointWithCalculatedResultToDatabase(Point point) {

    }
}